FROM nginx/unit:1.25.0-php8.0
COPY ./config.json /docker-entrypoint.d/config.json
COPY ./webapp /www 
EXPOSE 8000