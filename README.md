![](https://i.imgur.com/IxfVusz.jpg)
# Helm chart for deploy applications with Nginx-unit.
**by default, the application works with php 8*

Tree:
```
.
├── Dockerfile -------------- - includes your app code in webapp directory
├── README.md --------------- - this readme file
├── config.json ------------- - Nginx configuration file
├── helm -------------------- - Helm folder
│   ├── Chart.yaml ---------- - main Helm file
│   ├── templates ----------- - deployments folder
│   │   ├── deployment.yaml - - Nginx deployment
│   │   ├── ingress.yaml ---- - ingress deployment
│   │   └── service.yaml ---- - service deployment
│   └── values.yaml --------- - main values for Helm deplouments
└── webapp ------------------ - your application code folder
    └── index.php ----------- - your application code
```

Quickstart:
-
* clone
* add your app code in webapp folder
* configure config.json -*more info in https://unit.nginx.org/*
* add varibles in nginx-test/values.yaml
* push -*.gitlab-ci.yml build your container and pull in gitlab container repository*

- deploy: `helm your_app_name your_Chart.yaml_path`
- enjoy
